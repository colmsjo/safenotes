/* jshint undef: true, unused: false, curly: false, -W069 */

// fragments.js
// ============
//
// Jonas Colmsjö, http://fragmentsjs.github.io
//

var F = (function(){
  'use strict';


  var $ = document.getElementById.bind(document);

  // Setup
  // -----

  var noLogging_ = false;
  var filename_ = '';

  var init = function(options) {
    if (options.noLogging) {
      noLogging_ = options.noLogging;
    }

    if (options.filename) {
      filename_ = options.filename;
    }

    // setup screen navigation
    window.addEventListener('hashchange', route, false);
  };

  // Logging
  // -------

  var logError = null;
  var logDebug = null;
  var log = null;

  var logFunc_ = function(logWith, logId) {
    return function() {
      var args = Array.prototype.slice.call(arguments);

      if(noLogging_) {
        return;
      }

      args.forEach(function(arg){
        logWith(arg);
        if ($(logId)) {
          $(logId).innerHTML += arg + '<br>';
        }
      });
    };
  };

  var createErrorLogger = function(logWith, logId) {
    logError = logFunc_(logWith, logId);
    return logError;
  };

  var createDebugLogger = function(logWith, logId) {
    logDebug = logFunc_(logWith, logId);
    return logDebug;
  };

  var createLogger = function(logWith, logId) {
    log = logFunc_(logWith, logId);
    return log;
  };

  // Manage Screens
  // --------------

  var showScreen = function (newURL) {

    // hide current page
    var currentPage = document.getElementById('content').firstElementChild;
    if(currentPage) {
      document.getElementById('templates').appendChild(currentPage);
    }

    var newPage = document.getElementById(getHash(newURL).slice(1) + 'Screen');
    if(newPage) {
      document.getElementById('content').appendChild(newPage);
    } else {
      logError('Could not find:', getHash(newURL).slice(1) + 'Screen');
    }
  };

  var route = function (evt) {
    showScreen(evt.newURL, evt.oldURL);
  };

  var getHash = function (url) {
    var parser = document.createElement('a');
    parser.href = url;
    return parser.hash;
  };

  // use this if you need to parse the query/search string
  var parseQueryString = function (str) {
    return str.substring(1).split('&')
      .map(function (s) {
        return s.split('=');
      })
      .reduce(function (r, x) {
        r[x[0]] = x[1];
        return r;
      }, {});
  };

  // Manage touch screens
  // --------------------

  var getParentHash_ = function (el) {
    if (!el) {
      return null;
    }
    return (el.hash) ? el.hash : getParentHash_(el.parentElement);
  };

  var getParentHref_ = function (el) {
    if (!el) {
      return null;
    }
    return (el.href) ? el.href : getParentHref_(el.parentElement);
  };

  // avoid 300ms delay for touch screens
  var createTapFunc = function (cb) {
    return function(evt) {
      // prevent click from happening
      evt.preventDefault();

      // an element inside an anchor might have been tapped
      var hash = getParentHash_(evt.target);
      var href = getParentHref_(evt.target);

      // perform the desired action, i.e. show a screen etc.
      history.pushState(null, null, hash);
      cb(href);
    };
  };

  // Streams (reactive programming)
  // ------------------------------

  // Example of usage:
  //```
  //    var input = document.getElementById('input');
  //    var input2 = document.getElementById('input2');
  //    var results = document.getElementById('results');
  //
  //    Observable.fromEvent(input, 'keyup')
  //      .merge(Observable.fromEvent(input2, 'keyup'))
  //      .map(function(evt) {
  //        return evt.target.value;
  //      })
  //      .map(function(str) {
  //        results.value = str
  //      });
  //```

  var Observable = function(operation, arg) {
    this.operation_ = operation;
    if (operation === 'map') {
      this.fn_ = arg;
    }
    if (operation === 'merge') {
      this.mergeWith_ = arg;
    }
    this.observers_ = [];
  };

  Observable.prototype.register = function(observer) {
    this.observers_.push(observer);
  };

  Observable.prototype.notify = function(evt) {
    if (this.operation_ === 'map') {
      var tmpEvt = this.fn_(evt);
      evt = tmpEvt ? tmpEvt : evt;
    }

    this.observers_.forEach(function(o) {
      o.notify(evt);
    });
  };

  Observable.prototype.listen = function(element, event) {
    element.addEventListener(event, this.notify.bind(this));
  };

  Observable.fromEvent = function(element, event) {
    var observable = new Observable();
    observable.listen(element, event);
    return observable;
  };

  Observable.prototype.map = function(fn) {
    var observable2 = new Observable('map', fn);
    this.register(observable2);

    return observable2;
  };

  Observable.prototype.merge = function(observable2) {
    var observable3 = new Observable('merge', observable2);

    this.register(observable3);
    observable2.register(observable3);

    return observable3;
  };

  // Debounce/throttling
  // --------------------
  //
  // Example of usage:
  //```
  //    var myEfficientFn = debounce(function(evt) {
  //      console.log(evt);
  //     }, 250);
  //
  //     window.addEventListener('resize', myEfficientFn);
  //```

  var debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  // Styling functions
  // ------------------

  var getRemPixels = function () {
    var testElement = document.createElement("i");

    var important = "!important;";
    testElement.style.cssText = "position:absolute" + important +
                "visibility:hidden" + important +
                "width:1rem" + important +
                "font-size:1rem" + important +
                "padding:0" + important;

    document.documentElement.appendChild(testElement);

    var value = testElement.clientWidth;
    document.documentElement.removeChild(testElement);

    return value;
  };

  // height minus scrollbars (if any)
  // http://stackoverflow.com/questions/1248081/get-the-browser-viewport-dimensions-with-javascript
  var getViewportHeight = function() {
    return document.documentElement.clientHeight;
  };

  // public functions
  // ----------------
  return {
    $: $,
    init: init,

    createLogger: createLogger,
    createErrorLogger: createErrorLogger,
    createDebugLogger: createDebugLogger,

    showScreen: showScreen,

    createTapFunc: createTapFunc,

    Observable: Observable,
    debounce: debounce,

    getRemPixels: getRemPixels,
    getViewportHeight: getViewportHeight
  };

}());

// Export the module
// ------------------

window['fragmentsjs'] = F;
